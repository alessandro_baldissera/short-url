﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using PagedList.Mvc;

namespace ShortenLink.Models
{
    public class MainModel
    {
        public LinkModel LinkModel { get; set; }
        public IEnumerable<ShortenLink.URLS> UrlTable { get; set; }
}
}