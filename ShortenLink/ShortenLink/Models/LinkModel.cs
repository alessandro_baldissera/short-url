﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ShortenLink.Models
{
    public class LinkModel
    {
        [Required]
        [Display(Name = "Shorten your link")]
        public string Link { get; set; }
    }
}