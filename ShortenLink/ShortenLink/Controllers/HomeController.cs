﻿using ShortenLink.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using System.Security.Cryptography;
using System.Text;

namespace ShortenLink.Controllers
{
    public class HomeController : Controller
    {
        private static Random random = new Random();
        ShortenLink.ShortenLinkEntities db = new ShortenLinkEntities();

        public ActionResult Index(MainModel mm)
        {
            if (ModelState.IsValid)
            {
                URLS link = new URLS();
                HttpCookie cookie = Request.Cookies["VisitorShortenLink"];
                
                // caso in cui l'utente crea uno short URL
                if (Request.HttpMethod == "POST")
                {
                    if (!string.IsNullOrEmpty(mm.LinkModel.Link))
                    {
                        try
                        {
                            using (var context = new ShortenLinkEntities())
                            {
                                USERS_URLS uu = new USERS_URLS();
                                USERS user = new USERS();

                                if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
                                    user = context.USERS.Find(new Guid(cookie.Value));

                                // se l'utente è nuovo, gli assegno un Guid come cookie e salvo a db l'utente
                                // tengo salvati gli utenti che creano gli short URL nel caso in futuro si volesse fare analisi statistiche su di essi
                                if (cookie == null || string.IsNullOrEmpty(cookie.Value) || user == null)
                                {                                  
                                    user = new USERS();
                                    user.USERID = Guid.NewGuid();
                                    user.USERAGENT = Request.UserAgent;
                                    user = context.USERS.Add(user);

                                    cookie = new HttpCookie("VisitorShortenLink");
                                    cookie.Value = user.USERID.ToString();
                                    cookie.Expires = new DateTime(2038, 01, 19); // bug del 2038
                                    Response.SetCookie(cookie);
                                }

                                // generazione stringa casuale di 6 caratteri a base 62 per creare lo slug (62^6 possibili stringhe)
                                Random rnd = new Random();                                
                                string slug = RandomString(6);

                                // salvataggio dell'url a db 
                                link.URLID = slug;
                                link.NAME = mm.LinkModel.Link;
                                link.DATEINSERTED = DateTime.Now;
                                link.CLICK = 0;
                                link.VISIT = 0;

                                // per ogni url viene memorizzato da quale utente viene creato per poter generare la tabella UrlTable
                                link.CREATEDBY = user == null ? new Guid(cookie.Value) : user.USERID;
                                URLS u = context.URLS.Add(link);
                                context.SaveChanges();
                            }

                        }
                        catch (Exception ex) { };
                    }
                }

                // costruzione del modello UrlTable: ogni utente può vedere solo gli short url da lui creati
                if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
                    mm.UrlTable = db.URLS.Where(t => t.CREATEDBY == new Guid(cookie.Value)).ToList().OrderByDescending(s => s.DATEINSERTED).Take(10);
                else
                    mm.UrlTable = Enumerable.Empty<URLS>();
            }
            else
            {
                mm = new MainModel();
            }

            return View(mm);
        }

        // funzione che genera stringa casuale di lunghezza 6 a base 62
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghjklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        public ActionResult RedirectShortUrl(string slug) // slug
        {
            URLS link = db.URLS.Find(slug);
            try
            {

                // se lo short url non è presente a db rimando alla pagina di errore 404
                if (link == null)
                    return View("Error");

                string address = link.NAME;

                if (!String.IsNullOrEmpty(address))
                {
                    USERS user = new USERS();
                    Guid userId;

                    // richiedo il cookie all'utente
                    HttpCookie cookie = Request.Cookies["VisitorShortenLink"];

                    // incremento di uno il numero di click dello short url corrente
                    link.CLICK = link.CLICK + 1;
                    db.SaveChanges();

                    // se non è presente l'utente a db, creo l'utente altrimenti già presente
                    if (cookie == null || string.IsNullOrEmpty(cookie.Value))
                    {
                        user.USERID = Guid.NewGuid();
                        user.USERAGENT = Request.UserAgent; // per sviluppi futuri
                        user = db.USERS.Add(user);
                        userId = user.USERID;
                    }
                    else
                        userId = new Guid(cookie.Value);

                    // verifico se l'utente è la prima volta che ha visitato lo short url
                    var anyUserUrl = (from s in db.USERS_URLS
                                      where s.USERID == userId && s.URLID == slug
                                      select s.USERID);
                  
                    if (anyUserUrl.Count() == 0)
                    {
                        //non c'è corrispondenza tra visitatore (utente) e link, incremento di uno il numero di visite uniche
                        link.VISIT = link.VISIT + 1;
                        db.SaveChanges();

                        //creazione corrispondenza tra visitatore e url
                        USERS_URLS uu = new USERS_URLS();
                        uu.USERID = userId;
                        uu.URLID = slug;
                        uu.FIRSTDATE = DateTime.Now; // per sviluppi futuri
                        uu = db.USERS_URLS.Add(uu);

                        // assegno il cookie all'utente
                        cookie = new HttpCookie("VisitorShortenLink");
                        cookie.Value = userId.ToString();
                        cookie.Expires = new DateTime(2038, 01, 19); // bug del 2038
                        Response.SetCookie(cookie);
                    }

                    db.SaveChanges();


                    // reindirizzo l'utente in base allo short url
                    if (!link.NAME.StartsWith("http://") && !link.NAME.StartsWith("https://"))
                    {
                        address = "http://" + link.NAME;
                        return Redirect(address);
                    }
                    else
                    {
                        return Redirect(address);
                    }
                }
            }
            catch (Exception ex) { };
            return View();
        }
    }
}