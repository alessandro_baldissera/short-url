//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ShortenLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class USERS_URLS
    {
        public System.Guid USERID { get; set; }
        public string URLID { get; set; }
        public System.DateTime FIRSTDATE { get; set; }
    
        public virtual URLS URLS { get; set; }
        public virtual USERS USERS { get; set; }
    }
}
