﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ShortenLink.Startup))]
namespace ShortenLink
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
